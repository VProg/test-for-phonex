@import "vars"
@import "fonts"
@import "libs"

::selection
	background-color: $accent-color
	color: $text-color

html, body
	height: 100%
	width: 100%

html
	box-sizing: border-box

*, *::before, *::after
	box-sizing: inherit

body
	position: relative
	display: flex
	min-width: 320px
	font-family: $default-font
	font-size: 17px
	line-height: 1.4
	color: $text-color
	background-color: $body-bg-color
	overflow-x: hidden

.button
	width: 120px
	height: 40px
	font-size: 15px
	color: #fff
	border: 2px solid blue
	border-radius: 4px
	background-color: blue
	outline: none
	cursor: pointer
	transition: all .150s
	&:hover
		color: blue
		background-color: #fff


.fields-list
	list-style: none


.step-title
	margin: 0 0 15px
	font-size: 21px
	font-weight: normal
	text-align: center

.form-wrapper
	width: 600px
	margin: auto
	padding: 40px
	border: 1px solid #ccc
	border-radius: 8px
	background-color: #fff
	@media screen and (max-width: 576px)
		width: 100%
		border: none
		border-radius: 0

.fieldset
	display: none
	margin: 0
	padding: 0
	border: none
	&:first-of-type
		display: block

.form-row
	display: flex
	align-items: center
	justify-content: space-between
	flex-wrap: wrap

.form-col
	flex: 0 0 calc(50% - 10px)
	@media screen and (max-width: 576px)
		flex: 0 0 100%

.form-nav
	margin-top: 20px
	display: flex
	justify-content: center
	.button
		margin: 10px

.form-group
	position: relative
	width: 100%
	margin-bottom: 40px
//	max-width: 280px

.form-label
	position: absolute
	left: 0
	top: 0
	width: auto
	padding: 0 4px
	font-size: 15px
	line-height: 1
	color: #716d6d
	cursor: text
	transform: translate(7px, 15px)
	transform-origin: top left
	transition: all .150s
	z-index: 1


.form-control
	width: 100%
	height: 45px
	padding: 7px 10px
	font-size: 15px
	border: 1px solid #ccc
	border-radius: 4px
	background-color: #fff
	outline: none
	transition: border-color .150s
	&:focus
		padding: 6px 9px
		border: 2px solid blue
		& ~ .form-label
			//font-size: 13px
			color: blue
			background-color: #fff
		&:not(select) ~ .form-label
			transform: scale(.75) translate(15px,-50%)
	&.has-value
		& ~ .form-label
			background-color: #fff
			transform: scale(.75) translate(15px,-50%)
	&.is-invalid-input
		padding: 6px 9px
		border: 2px solid red
		&:focus ~ .form-label
			color: red
	&.disabled
		background-color: #ccc
		border-color: #ccc
	option:first-of-type
		display: none
.form-error
	position: absolute
	top: calc(100% + 5px)
	width: auto
	font-size: 13px
	line-height: 1
	color: red
	opacity: 0
	visibility: hidden
	transition: all .150s
	&.is-visible
		opacity: 1
		visibility: visible