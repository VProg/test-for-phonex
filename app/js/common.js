$.validator.addMethod('lettersOnly', function(value, element) {
    return this.optional(element) || /^[a-zA-Z ]+$/.test(value);
}, 'Letters only please.');

$.validator.addMethod('password', function(value, element) {
    return this.optional(element) || /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{0,}$/.test(value);
}, 'Required at list one number and uppercase and lowercase letters and at list onespecial character');

$(function() {

    var $formControl =  $('.form-control');

    if ($formControl.length) {
        //Floated label on page load if have value
        $formControl.each(function () {
            if ($(this).val() != null) {
                if ($(this).val().length > 0) {
                    $(this).addClass('has-value');
                } else {
                    $(this).removeClass('has-value');
                }
            }
        });

        //Floated label on change
        $formControl.on('change blur', function () {
            if ($(this).val().length > 0) {
                $(this).addClass('has-value');
            } else {
                $(this).removeClass('has-value');
            }
        });

    }

    var $registrationForm = $('#register-form');

    if ($registrationForm.length) {

        // Form validation
        $registrationForm.validate({
            ignored: ':hidden, select:hidden',
            focusInvalid: false,
            debug: true,
            validClass: 'is-valid-input',
            errorClass: 'is-invalid-input',
            errorElement: 'span',
            rules: {
                'first-name': {
                    required: true,
                    lettersOnly: true
                },
                'last-name': {
                    required: true,
                    lettersOnly: true
                },
                'login': {
                    required: true
                },
                'email' : {
                    required: true,
                    email: true
                },
                'password': {
                    required: true
                },
                'confirm-password': {
                    equalTo: '#password'
                },
                'department': {
                    required: true
                },
                'vacancy': {
                    required: true
                }
            },
            messages: {
                'confirm-password': {
                    equalTo: 'Must be equal to password.'
                }
            },
            errorPlacement: function(error, element) {
                error.appendTo($(element).parent('.form-group').find('.form-error'));
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element).parent('.form-group').find('.form-error').addClass('is-visible');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).addClass(validClass).removeClass(errorClass);
                $(element).parent('.form-group').find('.form-error').removeClass('is-visible');
            }
        });

        //Showing next step on button click
        var $formNextStepButton = $('.next');

        $formNextStepButton.on('click', function (e) {
            e.preventDefault();

            if (!$registrationForm.valid()) return;

            var nextStepNum = $(this).data('nextStep'),
                $nextStep = $('#step-' + nextStepNum),
                $currentStep = $(this).closest('.fieldset');

            if ($nextStep.attr('id') === 'step-3') {
                $nextStep.find('.first-name').append($registrationForm.find('[name="first-name"]').val());
                $nextStep.find('.last-name').append($registrationForm.find('[name="last-name"]').val());
                $nextStep.find('.login').append($registrationForm.find('[name="login"]').val());
                $nextStep.find('.email').append($registrationForm.find('[name="email"]').val());
                $nextStep.find('.company').append($registrationForm.find('[name="company-name"]').val());
                $nextStep.find('.department').append($registrationForm.find('[name="department"]').val());
                $nextStep.find('.vacancy').append($registrationForm.find('[name="vacancy"]').val());
            }

            $nextStep.show(400);
            $currentStep.hide(200);
        });

        // Related selects from json
        var json = {
            'departments': {
                'Sales' : [
                    'Sales Manager',
                    'Account Manager'
                ],
                'Marketing' : [
                    'Creative Manager',
                    'Marketing Coordinator',
                    'Content Writer'
                ],
                'Technology' : [
                    'Project Manager',
                    'Software Developer',
                    'PHP programmer',
                    'Front End',
                    'Quality Assurance'
                ]
            }
        },
            $departmentSelect = $('#department'),
            $vacancySelect = $('#vacancy');

        $departmentSelect.append('<option></option>');
        $.each(json.departments, function (key) {
            $departmentSelect.append('<option value="' + key + '" data-key="' + key + '">' + key + '</option>');
        });

        $vacancySelect
            .addClass('disabled')
            .prop('disabled', true);

        $departmentSelect.on('change', function () {
            var index = $(this).find('option:selected').data('key');

            $vacancySelect
                .empty()
                .append('<option></option>')
                .removeClass('disabled')
                .prop('disabled', false);

            $.each(json.departments[index], function (key, val) {
                $vacancySelect
                    .append('<option value="' + val + '" data-key="' + val + '">' + val + '</option>')
                    .change();
            });
        });

        //Submit handler of form(set fields to local  storage + clear form fields + alert 'thank you'
        $registrationForm.on('submit', function (e) {
           e.preventDefault();

           var formFields = {};

            $(this).find(':input:not(button)').each(function() {

                formFields[$(this).attr('id')] = $(this).val();

                localStorage.setItem('PhonexaForm', JSON.stringify(formFields));

                $(this).val('');
            });

          alert('Thank you! Fields successfully saved to localStorage.');
          window.location.reload();
        });
    }

});
